# my_gym_schedule

This is my beloved [Science of Jiu-Jitsu](https://sojj.at/) gym's iCal schedule of all the classes you can sign up for directly.
This little project parses the gym's website, extracts the schedule for the next 4 weeks and puts it into gym_schedule.ics file. 

It updates twice a day: at 6AM and at 2PM. I think it is reasonable enough to know possible changes for morning or evening sessions. Let me know if you think otherwise.

Use [this link](https://gitlab.com/alexandretsarev/my_gym_schedule/-/raw/main/gym_schedule.ics?ref_type=heads) to subscribe to it.