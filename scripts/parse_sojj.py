import requests
from bs4 import BeautifulSoup
from icalendar import Calendar, Event
from datetime import datetime, timedelta
import pytz
import hashlib

def fetch_schedule(date):
    url = f'https://gymdesk.com/widgets/schedule/getevents?date={date}&id=1886&schedule_id=1536&direction=next'
    headers = {
        'accept': '*/*',
        'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
        'cookie': 'BJJSESSID=qqp7kncgj70pdgp19k1f4mi1gi',
        'referer': 'https://gymdesk.com/widgets/schedule/render/gym/lNnEl',
        'sec-ch-ua': '"Google Chrome";v="123", "Not:A-Brand";v="8", "Chromium";v="123"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"macOS"',
        'sec-fetch-dest': 'empty',
        'sec-fetch-mode': 'cors',
        'sec-fetch-site': 'same-origin',
        'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36',
        'x-requested-with': 'XMLHttpRequest',
    }
    response = requests.get(url, headers=headers)
    return response.text

def fetch_and_update_calendar():
    cal = Calendar()
    cal.add('prodid', '-//Gym Schedule//mxm.dk//')
    cal.add('version', '2.0')
    timezone = 'Europe/Vienna'
    tz = pytz.timezone(timezone)
    
    now = datetime.now(tz) 
    
    # Get the date of the Monday of the current week
    monday_this_week = now - timedelta(days=now.weekday()) - timedelta(weeks=1)

    schedule = []
    event_data_strings = []
    #TODO: make it for longer time period.
    for week in range(4): # looking for the period of the next 2 weeks
        
        week_date_str = (monday_this_week + timedelta(weeks=week)).strftime("%Y-%m-%d")

        html_content = fetch_schedule(week_date_str)
        soup = BeautifulSoup(html_content, 'html.parser')
        
        days = soup.find_all('div', class_='day')
        for day in days:
            day_date_str = day['attr-date']
            day_date = datetime.strptime(day_date_str, "%Y-%m-%d").date()
            
            events = day.find_all('div', class_='event')
            for event in events:
                start_time_str = event['attr-time']
                start_datetime_str = f"{day_date_str} {start_time_str}"
                start_time = tz.localize(datetime.strptime(start_datetime_str, "%Y-%m-%d %H:%M:%S"))
                

                if start_time <= now - timedelta(days=1): 
                    continue  # Skip past events
                
                duration_str = event.find('small').text
                times = duration_str.split(' - ')
                end_time_str = f"{day_date_str} {times[1]}"
                end_time = tz.localize(datetime.strptime(end_time_str, "%Y-%m-%d %H:%M"))
                
                summary = event.find('h3').get_text(strip=True)

                # Check if the class is canceled
                if 'canceled' in event['class']:
                    instructor = "CANCELLED"
                else:
                    instructor = event.find('em').get_text(strip=True) if event.find('em') else 'Unknown'
                  
                event_data = f"{summary}{start_time}{end_time}{instructor}"
                event_data_strings.append(event_data)
                
                ical_event = Event()
                ical_event.add('summary', summary)
                ical_event.add('dtstart', start_time)
                ical_event.add('dtend', end_time)
                ical_event.add('location', instructor)
                cal.add_component(ical_event)

    # Generate a hash of the concatenated event data strings
    current_hash = hashlib.md5(''.join(event_data_strings).encode()).hexdigest()
    
    # Compare the current hash with the previously stored hash
    try:
        with open('schedule_hash.txt', 'r') as f:
            previous_hash = f.read().strip()
    except FileNotFoundError:
        previous_hash = ""

    if current_hash != previous_hash:
        # Save the iCal file if there are updates
        file_path = 'gym_schedule.ics'
        with open(file_path, 'wb') as f:
            f.write(cal.to_ical())
        print(f"iCal file created or updated at {file_path}")

        # Update the stored hash
        with open('schedule_hash.txt', 'w') as f:
            f.write(current_hash)
    else:
        print("No updates to the schedule.")

# Run the function
fetch_and_update_calendar()
